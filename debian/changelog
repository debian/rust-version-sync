rust-version-sync (0.9.5-7) unstable; urgency=medium

  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + improve usage comment
  * update watch file:
    + improve filename and upstream version mangling
    + use Github API
    + drop unused repack suffix
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 08 Feb 2025 22:28:03 +0100

rust-version-sync (0.9.5-6) unstable; urgency=medium

  * tighten (build-)dependency for crate pulldown-cmark;
    closes: bug#1091208

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 28 Dec 2024 01:57:35 +0100

rust-version-sync (0.9.5-5) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * stop mention dh-cargo in long description
  * add patch cherry-picked upstream
    to use newer branch of crate pulldown-cmark;
    add patch 2001
    to accept older branch of crate pulldown-cmark;
    relax (build-)dependency for crate pulldown-cmark;
    closes: bug#1071789, thanks to Zixing Liu

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 18 Dec 2024 00:35:32 +0100

rust-version-sync (0.9.5-4) unstable; urgency=medium

  * add patch cherry-picked upstream
    to use newer branch of crate toml;
    drop patch 1001;
    tighten (build-)dependencies for crate toml
  * update copyright info: fix file path
  * declare compliance with Debian Policy 4.7.0
  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)
  * relax to autopkgtest-depend unversioned
    when version is satisfied in Debian stable

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 14 Jul 2024 01:29:01 +0200

rust-version-sync (0.9.5-3) unstable; urgency=medium

  * drop patch 2001, obsoleted by Debian package changes
  * add patch 1001 to accept newer branch of toml;
    relax (build-)dependency for crate toml;
    closes: bug#1061374, thanks to Peter Green
  * update dh-cargo fork
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 23 Jan 2024 17:15:06 +0100

rust-version-sync (0.9.5-2) unstable; urgency=medium

  * fix: bump (build-)dependency for crate toml

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 16 Oct 2023 22:25:04 +0200

rust-version-sync (0.9.5-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * bump project version in virtual packages and autopkgtests
  * update dh-cargo fork
  * update patches
  * bump (build-)dependencies for crate syn
  * relax to build-depend unversioned on dh-cargo
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 16 Oct 2023 18:35:26 +0200

rust-version-sync (0.9.4-5) unstable; urgency=medium

  * renumber patch 2001 -> 1001;
    update DEP-3 patch headers
  * update dh-cargo fork;
    closes: bug#1045224, thanks to Lucas Nussbaum

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 20 Aug 2023 11:39:01 +0200

rust-version-sync (0.9.4-4) unstable; urgency=medium

  * tighten autopkgtests
  * stop superfluously provide
    virtual un- or zero-versioned feature packages
  * change binary library package to be arch-independent
  * declare compliance with Debian Policy 4.6.2
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 03 Feb 2023 11:28:42 +0100

rust-version-sync (0.9.4-3) unstable; urgency=medium

  * re-release for building with auto-builder

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 17:04:00 +0200

rust-version-sync (0.9.4-2) unstable; urgency=medium

  * avoid metapackages,
    using the equivalent of debcargo setting collapse_features=true

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 10:09:19 +0200

rust-version-sync (0.9.4-1) unstable; urgency=low

  * initial Release;
    closes: Bug#1022887

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 27 Oct 2022 13:13:27 +0200
