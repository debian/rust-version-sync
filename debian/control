Source: rust-version-sync
Section: rust
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-rust,
 librust-proc-macro2-1+span-locations-dev,
 librust-pulldown-cmark-dev (>= 0.10),
 librust-regex-1+std-dev,
 librust-regex-1+unicode-dev,
 librust-semver-1+default-dev,
 librust-syn-2+full-dev,
 librust-syn-2+parsing-dev,
 librust-syn-2+printing-dev,
 librust-tempfile-3+default-dev,
 librust-toml-0.8+default-dev,
 librust-url-2+default-dev,
Maintainer: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian/rust-version-sync.git
Vcs-Browser: https://salsa.debian.org/debian/rust-version-sync
Homepage: https://github.com/mgeisler/version-sync
Rules-Requires-Root: no

Package: librust-version-sync-dev
Architecture: all
Multi-Arch: foreign
Depends:
 librust-proc-macro2-1+span-locations-dev,
 librust-pulldown-cmark-dev (>= 0.10),
 librust-regex-1+std-dev,
 librust-regex-1+unicode-dev,
 librust-semver-1+default-dev,
 librust-syn-2+full-dev,
 librust-syn-2+parsing-dev,
 librust-syn-2+printing-dev,
 librust-toml-0.8+default-dev,
 librust-url-2+default-dev,
 ${misc:Depends},
Provides:
 librust-version-sync-0-dev (= ${binary:Version}),
 librust-version-sync-0.9+contains-regex-dev (= ${binary:Version}),
 librust-version-sync-0.9+default-dev (= ${binary:Version}),
 librust-version-sync-0.9+html-root-url-updated-dev (= ${binary:Version}),
 librust-version-sync-0.9+markdown-deps-updated-dev (= ${binary:Version}),
 librust-version-sync-0.9-dev (= ${binary:Version}),
 librust-version-sync-0.9.5-dev (= ${binary:Version}),
Description: ensure packaging strings match crate metadata - Rust source code
 Rust projects typically reference the crate version number
 in several places, such as the "README.md" file.
 The version-sync crate makes it easy to add an integration test
 that checks that "README.md" is updated
 when the crate version changes.
 .
 This package contains the source for the Rust version-sync crate,
 for use with cargo.
